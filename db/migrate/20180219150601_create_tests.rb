class CreateTests < ActiveRecord::Migration[5.1]
  def change
    create_table :tests do |t|
      t.string :name
      t.integer :age
      t.text :introduce

      t.timestamps
    end
  end
end
