json.extract! test, :id, :name, :age, :introduce, :created_at, :updated_at
json.url test_url(test, format: :json)
